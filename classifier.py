import numpy

class Classifier:
    def __init__(self, learning_data):
        self.ermit = numpy.array([1,4,4,16])
        self.learn(learning_data)

    def learn(self, learning_data):
        self.function = numpy.array([0,0,0,0])
        for vector, classFlag in learning_data:
            potential = Classifier.calculateErmitFunction(self.ermit, vector)
            functionResult = sum(Classifier.calculateErmitFunction(self.function, vector))
            correction = 1 if (functionResult<= 0 and classFlag==1) else (-1 if (functionResult> 0 and classFlag==0) else 0)
            self.function += correction*potential

    def getClassFlag(self,vector):
        return int(sum(Classifier.calculateErmitFunction(self.ermit, vector)) > 0)

    @staticmethod
    def calculateErmitFunction(currFunction, vector):
        function = currFunction.copy()
        function[1] *= vector[0]
        function[2] *= vector[1]
        function[3] *= numpy.prod(vector)
        return function