from sys import argv
from collections import defaultdict
from random import uniform
from classifier import Classifier
from plotBuilder import drawData
import os

def main():
    lerningDataFilePath = os.path.dirname(os.path.abspath(__file__))+'\\data.txt'
    learningData = readLearingData(lerningDataFilePath)
    classifier = Classifier(learningData)
    interval = -10, 10
    resultData = defaultdict(list)
    for vector, classFlag in learningData:
        resultData[classFlag].append(vector)
    drawData(classifier.function, resultData)
    for _ in range(1000):
        vector = (uniform(*interval), uniform(*interval))
        resultData[classifier.getClassFlag(vector)].append(vector)
    drawData(classifier.function, resultData)

def readLearingData(lerningDataFilePath):
    result = []
    with open(lerningDataFilePath, 'r') as dataFile:
        for line in dataFile.readlines():
            data = line.split()
            vector = list(map(int, data[:2]))
            result.append((vector, int(data[2])))
    return result

main()