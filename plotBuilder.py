import matplotlib.pyplot as plt
from functools import reduce
import numpy
from operator import add

STEP = 0.001

def functionBreak(coefs):
    return -coefs[2] / coefs[3] if coefs[3] else None
    
def getFunction(coefs):
    def f(x):
        return (-coefs[1] * x - coefs[0]) / (coefs[3] * x + coefs[2])
    return f

def getLimits(data):
    allVectors = reduce(add, data.values())
    xs = list(map(lambda x: x[0], allVectors))
    ys = list(map(lambda x: x[1], allVectors))
    return {
        'x': (min(xs) - 1, max(xs) + 1),
        'y': (min(ys) - 1, max(ys) + 1),
    }

def drawData(coefs, data):
    limits = getLimits(data)
    plt.xlim(limits['x'])
    plt.ylim(limits['y'])
    plot(coefs, limits['x'])
    drawDots(data)
    plt.show()


def drawDots(data):
    colors = {1: 'blue', 0: 'green'}
    for classFlag, vectors in data.items():
        xs = list(map(lambda x: x[0], vectors))
        ys = list(map(lambda x: x[1], vectors))
        plt.scatter(xs, ys, c=colors[classFlag], marker='.')


def isIntersectingLines(coefs):
    return coefs[2] * coefs[1] == coefs[3] * coefs[0]


def plot(coefs, interval):
    function = getFunction(coefs)
    breakPoint = functionBreak(coefs)
    intervalStart, intervalEnd = interval
    if breakPoint is not None:
        arange = numpy.arange(intervalStart, breakPoint - STEP, STEP)
        plt.plot(arange, function(arange), color='b')
        arange = numpy.arange(breakPoint + STEP, intervalEnd, STEP)
        plt.plot(arange, function(arange), color='b')
        if isIntersectingLines(coefs):
            plt.axvline(breakPoint, color='b')
    else:
        arange = numpy.arange(intervalStart, intervalEnd, STEP)
        plt.plot(arange, function(arange), color='b')
